import { app, BrowserWindow, nativeTheme, screen } from "electron";
import { initialize, enable } from "@electron/remote/main"; // <-- add this
import path from "path";
import os from "os";

initialize(); // <-- add this

// needed in case process is undefined under Linux
const platform = process.platform || os.platform();

try {
  if (platform === "win32" && nativeTheme.shouldUseDarkColors === true) {
    require("fs").unlinkSync(
      path.join(app.getPath("userData"), "DevTools Extensions")
    );
  }
} catch (_) {}

let mainWindow;

function createWindow() {
  /**
   * Initial window options
   */
  const { width, height } = screen.getPrimaryDisplay().workAreaSize;

  mainWindow = new BrowserWindow({
    icon: path.resolve(__dirname, "icons/icon.png"), // tray icon
    width: width,
    height: height,
    minWidth: 800,
    minHeight: 700,
    useContentSize: true,
    frame: false, // <-- add this
    webPreferences: {
      contextIsolation: true,
      sandbox: false, // <-- to be able to import @electron/remote in preload script
      // More info: https://v2.quasar.dev/quasar-cli-webpack/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD),
    },
  });

  mainWindow.webContents.once('dom-ready', () => {
    const zoomFactor = 1; // Adjust the value as needed

    mainWindow.webContents.zoomFactor = zoomFactor;
    mainWindow.show();
  });

  enable(mainWindow.webContents); // <-- add this

  mainWindow.loadURL(process.env.APP_URL);

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    mainWindow.webContents.openDevTools();
  } else {
    // we're on production; no access to devtools pls
    mainWindow.webContents.on("devtools-opened", () => {
      mainWindow.webContents.closeDevTools();
    });
  }

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
  if (platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});

export const publicFolder = path.resolve(
  __dirname,
  process.env.QUASAR_PUBLIC_FOLDER
);
