import { ref } from 'vue';
// this file contains the data to be across all the pages of the project


let url = "";
const userurl = localStorage.getItem('userURL');
if (userurl) {
  url= JSON.parse(userurl);
  console.log("store",url);
}

let token = "";
const access = localStorage.getItem('userAccessToken');
if (access){
  token = JSON.parse(access);
}

let userOptions = [];
const op = localStorage.getItem('options');
if (op){
  userOptions = JSON.parse(op);
}

let userGroup = [];
const gp = localStorage.getItem('group');
if (gp){
  userGroup = JSON.parse(gp);
}

let dict = {};
const dst = localStorage.getItem('dictionary');
if (dst){
  dict = JSON.parse(dst);
}

let hourValuesDict = {};
const hvalues = localStorage.getItem('hourValues');
if (hvalues){
  hourValuesDict = JSON.parse(hvalues);
}

export const userURL = ref(url);
export const userAccessToken = ref(token);
export const  options = ref(userOptions);
export const  group = ref(userGroup);
export const dictionary = ref(dict);
export const hourValues = ref(hourValuesDict)

