import { boot } from 'quasar/wrappers';

export default boot(({ Vue }) => {
  Vue.prototype.$appVersion = process.env.APP_VERSION;
});
